\documentclass[11pt]{article}
\usepackage[authoryear,round,longnamesfirst]{natbib}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
%\usepackage{microtype}
\usepackage{todonotes}
\usepackage[nolist,nohyperlinks]{acronym}
\usepackage{parskip}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{setspace}
\usepackage{indentfirst}
\usepackage{multirow}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{booktabs}
\usepackage{amsmath}
\setlength{\parindent}{1cm}
\usepackage[section,subsection,subsubsection]{extraplaceins}
\newcommand*{\MyIndent}{\hspace*{0.5cm}}
\setlength{\tabulinesep}{5pt}
\usepackage{float}
\usepackage[small]{titlesec}
%\titlespacing\section{0pt}{12pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\usepackage{enumitem}
%\usepackage{palatino}
\hyphenpenalty=1000
\exhyphenpenalty=1000
\usepackage[section,subsection,subsubsection]{extraplaceins}
\usepackage [autostyle, english = american]{csquotes}
\MakeOuterQuote{"}

\begin{document}

<<load, echo=FALSE, include=FALSE>>=
require(ggplot2)
require(knitr)
@


\input{./titlepage}



% The rest of the front pages should contain no headers and be numbered using Roman numerals starting with `ii'
\pagestyle{plain}
\setcounter{page}{2}

\cleardoublepage % Ends the current page and causes all figures and tables that have so far appeared in the input to be printed.


\section{Introduction} 

Aggressive behaviour is defined as "an overt act, involving the deliver of noxious stimuli to (but not necessarily aimed at) another organism, object or self, which is clearly not accidental" \citep{Ryden:1988}. In nursing home and other residential long-term care settings, aggressive behaviours are common, as prevalence studies estimate that 45-53\% of of residents show some signs of aggression \citep{Hirdes:2011fk, Shah:2000}. Examples of aggressive behaviours include agitation, shouting or abusive vocalizations, physical abuse, socially disruptive behaviours, resistance to care, and self-injurious behaviour \citep{Pulsford:2006, Perlman:2008rw}. The prevalence of these aggressive sub-syndromes varies. For example, \citet{Majic:2012} found that among long-term care residents with dementia, 76\% were verbally aggressive, while only 29\% were physically aggressive.

Resident attributes associated with aggressive behaviour are well studied in literature. Risk factors for aggressive behaviour among older institutionalized adults include male gender, cognitive impairment, dementia, depression, delusional thought, provision of psychotropic medications and pain \citep{Shah:2000, Pulsford:2006, Gustafsson:2013, Ahn:2014, Ishii:2010,Menon:2001, Majic:2012, Chan:2006a, Cassie:2012}. In addition, organizational factors that may increase the likelihood of aggressive behaviours include large facility size, low staff to resident ratios, and location in a non-urban setting \citep{Cassie:2012}.

Efforts to manage aggressive behaviours among residential long-term care residents should be made as these behaviours are associated with diminished quality of life for residents and caregivers \citep{Majic:2012}. \citet{Zeller:2012} estimated that as many as 80\% of caregivers experienced aggressive behaviours in the past twelve months. These behaviours may affect job satisfaction, professional burden, and place caregivers at risk of burn out \citep{Majic:2012, Zeller:2014}. Recommended measures to provide appropriate care and support for residents with aggressive behaviours include the study of individual antecedents to aggressive behaviour, reminiscence work, alternative bathing practices such as person-centred showering and towel bath, and release from indoor confinement \citep{Zeller:2014, Sloane:2004, Enmarker:2011}. 

More specific interventions aimed at addressing underlying risk factors for aggressive behaviour have also been studied. Among patients with recent stroke, successful treatment of depression with antidepressant medications resulted in a significant treatment response towards reduced aggression \citep{Chan:2006a}. In a study of patients with probable Alzheimer's disease and depressive episodes, treatment with antidepressant medications resulted in a decrease in dementia-associated behavioural disturbances \citep{Lyketsos:2003}. Given that between 18\% and 51\% of individuals in Canadian residential long-term care facilities show signs of symptoms of clinical depression \citep{Hirdes:2011fk}, successful treatment and management of depression may reduce the incidence of aggressive behaviours in this population. 

Though many studies have elucidated resident and organizational factors associated with aggressive behaviour in residential care settings, few studies have examined trajectories of change with respect to these problematic behaviours. The purpose of this study is to describe the effect of depression on change in aggressive behaviour over time among residential long-term care residents.   

\section{Methods}

\subsection{Data Source}

The primary data source for this study are interRAI Resident Assessment Instrument Minimum Data Set 2.0 (MDS 2.0) assessments from the Continuing Care Reporting System (CCRS) data repository maintained by the Canadian Institute of Health Information (CIHI). The MDS 2.0 is a comprehensive clinical assessment used globally to evaluate individuals residing in residential care settings across a broad range of health domains including physical functioning, cognition, mood and behaviour, social functioning, diseases and conditions, health service and medication utilization \citep{Bernabei:2008lq, Gray:2009kt, Hirdes:2008uq}. In Ontario, Canada, the use of the MDS 2.0 assessment in residential long-term care facilities was legislated beginning July 1st, 2005. Residents are assessed using the instrument within two weeks of admission, and are re-assessed every 90 days thereafter. 

Embedded within the MDS 2.0 assessment are numerous validated outcome scales and indices, including the Aggressive Behaviour Scale (ABS), Activities of Daily Living Hierarchy Scale (ADL-H), Cognitive Performance Scale (CPS), Depression Rating Scale (DRS), Index of Social Engagement (ISE), and the Pain Scale. The ABS serves as an overall measure of aggressive behaviour based on the frequency of occurrence of four distinct aggressive behaviours: verbal abuse, physical abuse, socially disruptive behaviour and resistance to care \citep{Perlman:2008rw}. The ABS has been validated against the aggression sub-scale of the Cohen-Mansfield Agitation Inventory (CMAI) \citep{Perlman:2008rw}. ABS scores range from 0 to 12. The ADL-H is a hierarchical measure of functional performance based on an individual's capacity to complete early and late loss activities of daily living \citep{Fries:1994nk}. CPS is a 6 point measure of cognitive impairment that has been validated against the Mini-Mental State Examination (MMSE) \citep{Morris:1994ve, Hartmaier:1995fq}. Using items of mood and behaviour, the DRS is used to screen for clinical depression \citep{Burrows:2000qf}. The DRS ranges from 0 to 14, and scores greater than three are predictive of minor or major clinical depression \citep{Burrows:2000qf}. The DRS scale is highly correlated with Hamilton Rating Scale for Depression (HAM-D) and has demonstrated strong predictive accuracy for psychiatrist's depression diagnoses \citep{Burrows:2000qf}. The Index of Social Engagement (ISE) assesses social behaviour through engagement and participation in social activities \citep{Mor:1995ta,Gerritsen:2008sp}. It ranges from 0 to 6. Finally, the pain scale takes into account the frequency and severity of pain that an individual experiences to construct a three point summary measure of pain \citep{Fries:2001tg}. 


\subsection{Sample}

A sample of 16,129 individuals in residential long-term care from 38 facilities located within Toronto, Ontario, Canada were included in this study. Admission assessments were selected as the index assessment. When available, re-assessments at 180 day intervals from the index assessment were selected to measure change over time. Due to limits of computation capacity, only the first four re-assessment intervals for each resident were selected. Figure~\ref{fig:times} presents the number of assessments available at each re-assessment interval. This sample includes residents assessed with the MDS 2.0 assessment between August 2nd, 2005 and March 31st, 2014.

\begin{center}
\begin{minipage}[c]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{assess}
	\captionof{figure}{Number of assessments available at each time interval.}
	\label{fig:times}
\end{minipage}
\end{center}
\medskip

\subsection{Variables}

\subsubsection{Dependent Variable}

The dependent variable in this study was aggressive behaviour, as measured by the ABS. For the proposes of this study, ABS scores were factored into four mutually exclusive ordinal groups: no signs of aggression (ABS = 0), mild signs of aggression (ABS = 1-2), moderate signs of aggression (ABS = 3-5), and severe signs of aggression (ABS = 6+). 

\subsubsection{Independent Variables}

Independent variables included resident demographic characteristics such age, gender, and marital status, select diagnoses \& conditions, treatments, therapies, medications, and indicators of psychosocial wellbeing. For modelling purposes, given that DRS scores of three or greater are indicative of probable clinical depression, the DRS was factored into a binary variable called \emph{Binary DRS}. Residents with DRS scores of 3+ were assigned to the "Depressed" group. 

\subsection{Statistical Analyses}

Descriptive statistics comparing residents on admission in the various ABS groups were computed. Group differences were ascertained using the Chi-square frequency test using a 5\% $\alpha$. Subsequently, a cumulative non-proportional odds mixed model was used to estimate the effects of depression on aggressive behaviour group inclusion over time. Gender and CPS was included in the model to control for the their effects on aggression group inclusion. 

\subsubsection{Notation}

The following notation is used when describing the cumulative non-proportional odds mixed model:
\begin{itemize} 
	\item $\eta_{ij}$ denotes the $i^{th}$ resident's odds of inclusion into a specific aggression severity group at the $j^{th}$ re-assessment interval
	\item $CPS_{ij}$ denotes the $i^{th}$ resident's CPS score at the $j^{th}$ re-assessment interval
	\item $BinaryDRS_{ij}$ denotes the $i^{th}$ resident's BinaryDRS score at the $j^{th}$ re-assessment interval
	\item $CPS_{ij}$ denotes the $i^{th}$ resident's CPS score at the  $j^{th}$ re-assessment interval
	\item $Female_{i}$ denotes the $i^{th}$ resident's Gender score at admission; where Female = 0 represents men and Female = 1 represents women
\end{itemize} 

\subsubsection{Model and Assumptions}

The following model will be used to estimate the effect of depression status on odds of aggression group inclusion over time:

\begin{equation}\label{overall}
	\eta_{ij} = \pi_{0i} + \pi_{1i}Time_{ij} + \pi_{2i}CPS_{ij} + \pi_{3i}BinaryDRS_{ij} + \pi_{4i}BinaryDRS_{ij}*Time_{ij} 
\end{equation}
\begin{subequations}
	\begin{align}
		\pi_{0i} = \gamma_{00} + \gamma_{01}Female_{i} + \zeta_{0i} \\
	\pi_{1i} = \gamma_{10} + \zeta_{1i} \\
	\pi_{2i} = \gamma_{20} \\
	\pi_{3i} = \gamma_{30} \\
	\pi_{4i} = \gamma_{41} 
\end{align}
\end{subequations}

Preliminary analyses suggest that compared to a fixed effects model, a random intercepts and slopes model is most suitable. Violation of the proportional odds assumption was determined through visual inspection of effect plots.  

\section{Results}
\subsection{Descriptive Statistics} 

Table~\ref{tab:soc} presents basic patient attributes at admission by aggression group. Attributes presented include sociodemographic information, diagnoses and conditions, indicators of psychosocial well-being, and select treatments and programs. The prevalence of cancer, traumatic brain injury, anxiety, schizophrenia, urinary tract infection in last 30 days, and unstable health condition generally increased as residents were classified into more severe aggression groups. With more severe aggression group classification, residents were less likely to pursue involvement in facility activities, and were more likely to be in conflict with staff or fail to adjust easily to change in routines. 

Residents in the severe aggression group were most likely to receive care in a dementia care unit, be enrolled in a behaviour evaluation program, or require deliberate environment modifications to address mood or behaviour patterns. Residents in increasingly severe aggressive behaviour groups were more likely to receive antidepressant, antipsychotic, and anxiolytic medications (Figure~\ref{tab:meds}).


\input{./tables/sociodemographics}

Admission scale scores distributions for residents in the various aggression severity groups are presented in Table~\ref{tab:scales}. As residents were classified into more severe aggression groups, level of functional impairment in activities of daily living, as measured by the ADL-H scale, increased. Similarly, based on CPS, residents in more severe aggression groups were more likely to be moderately or severely cognitively impaired. The prevalence of clinically significant signs and symptoms of depression increased with aggression group severity. Social engagement,measured by the ISE, was greatest among those in the least severe aggression groups. Finally, though statistically significant, there was little difference in Pain Scale scores between aggression severity groups. 

\input{./tables/scales}

The frequency of various aggressive behaviours at baseline across aggression severity groups are presented in Table~\ref{tab:abuseitems}. Given that these items are used in the calculation of the ABS scales, as the aggression group severity increases, the frequency of these various aggressive behaviours increases. Among residents classified into the severe aggression group, daily resistance to care was most commonly exhibited, as 67\% of residents exhibited this behaviour daily. Thirty-eight percent of residents classified in the severe aggression group were verbally abusive on a daily basis, followed by 35\% displaying socially inappropriate behaviour and 26\% exercising physical abuse. 

\input{./tables/absitems}

\FloatBarrier

\subsection{Non-Proportional Odds Mixed Model}

Table~\ref{tab:depressionmodel} presents the results of the cumulative non-proportional odds mixed model predicting aggressive behaviour severity group inclusion. This random intercept and slope model clearly indicates that there is significant individual heterogeneity with respect to both intercept and slope. All covariates including gender, binary DRS, CPS and the interaction effect between binary DRS and time were statistically significant with an $\alpha$ of 5\%. Mean intercepts for inclusion in the various aggressive behaviour severity groups were -2.5254 (\emph{Mild or Worse vs. None}), -4.5467 (\emph{Moderate or Worse vs. Mild or Better}), and -6.7095 (\emph{Severe vs. Moderate or Better}). After accounting for gender and level of cognitive impairment (CPS) at each given assessment interval, average slopes for the effect of depression (Binary DRS) were 0.5432 (\emph{Mild or Worse vs. None}), 0.5914 (\emph{Moderate or Worse vs. Mild or Better}), and 0.6213 (\emph{Severe vs. Moderate or Better}). This positive effect of depression over time is depicted in Figure~\ref{fig:depressionplot}. Within all aggressive behaviour severity groups, odds of inclusion in the more severe aggressive behaviour group increase exponentially over time. Comparing across aggressive behaviour severity groups, at each time point the non-proportional odds assumption is satisfied. For example, for patients that shows signs of depression at the 4th re-assessment interval (length of stay 720 days), odds of inclusion in the \emph{Moderate or Worse vs. Mild or Better} group is 1.3 times greater than inclusion in the \emph{Moderate or Worse vs. Mild or Better}. The odds of inclusion in the \emph{Severe vs. Moderate or Better} group are 1.14 times greater than the odds of inclusion in the \emph{Moderate or Worse vs. Mild or Better} group. 

\input{./tables/depressionmodel}

<<depressionplot, child='plots/depressionplot.rnw'>>= 
@

\section{Discussion}

The results of this study clearly indicate that long-term care residents with signs and symptoms of depression are at greater risk of inclusion into more severe aggressive behaviour groups at each given re-assessment point between admission and 720 days of stay. Significant positive slope estimates indicate that risk of more severe aggressive behaviour group inclusion increases over time for individual in residential long-term care. Although other studies have established depression as a risk factor for aggressive behaviour in this population of institutionalized adults \citep{Ishii:2010,Menon:2001, Majic:2012, Chan:2006a, Cassie:2012}, this study is among the first to describe the effect of depression on change in risk over time.

Results from this study have clear and directions implications for the prevention and management of aggressive behaviours in residential long-term care settings. Previous studies have demonstrated that depressive behaviours in older adults are amenable to intervention \citep{Frazer:2005}, and successful treatment of depression has been shown to positivity affect the severity of aggressive behaviours \citep{Chan:2006a, Lyketsos:2003}. Established best practice guidelines for the detection and treatment of depression in this population should be used to enhance resident quality of life and reduce the burden of aggressive behaviours. In residential long-term care populations where the interRAI standardized assessment system has been implemented, care planners may make use of the DRS scale and the Depression Clinical Assessment Protocol (CAP) \todo{insert cap citation} to aide in identification and the development of care plans to address depression. Future studies should aim to evaluate the effect of both pharmacological and non-pharmacological treatments for depression on aggressive behaviour trajectories over time. 

A strength of this research is the inclusion of gender and cognitive performance covariates at each given re-assessment interval. Given that previous studies have shown that both gender and cognitive impairment are risk factors for depression and aggressive behaviour \todo{insert citation for the effect of gender and cognitive impairment}, inclusion of these of covariates controls for their individual effects on aggressive behaviour at a given re-assessment interval. 

This study is limited with respect to its operationalization of depression diagnosis in this patient population. Though the DRS has been validated as a predictor of a probable clinical depression diagnosis, it is not a substitute for assessment by a trained clinician. The MDS 2.0 assessment does include a separate item for the identification of residents diagnoses with depression, however depression is commonly under-diagnosed and reported in this population. Despite this limitation, the use of the DRS demonstrates that items involved the assessment of mood disturbance have utility in identifying residents at greater risk of increasing aggressive behaviour over time. 

\section{Conclusion}

This study has shown that signs of symptoms of depression are significant predictors of increased aggressive behaviour severity over time in residential long-term care settings. Though additional work is necessary to identify the effect of common depression treatments on change in aggressive behaviour over time, standardized comprehensive assessments should be used to identify those residents at greatest risk of displaying aggressive behaviour. 

\section{Acknowledgements} 

The author would like to acknowledge the technical assistance and support of Dr. Ashok Chaurasia in establishing the analytic framework used in this work. 









\cleardoublepage % This is needed if the book class is used, to place the anchor in the correct page,
                 % because the bibliography will start on its own page.
                 % Use \clearpage instead if the document class uses the "oneside" argument
\phantomsection  % With hyperref package, enables hyperlinking from the table of contents to bibliography             
% The following statement causes the title "References" to be used for the bibliography section:
\renewcommand*{\bibname}{References}

% Add the References to the Table of Contents
%\addcontentsline{toc}{chapter}{\textbf{References}}
\bibliography{20160416_HSG720_Final}{}
\bibliographystyle{apalike}

%\renewcommand*{\bibname}{References}
%
%% Add the References to the Table of Contents
%\addcontentsline{toc}{chapter}{\textbf{References}}
%\newpage
%\bibliography{bibdesk}{}
%\bibliographystyle{apalike}
\end{document}
